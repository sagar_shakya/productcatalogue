@extends('app')

@section('content')
<div class="box box-info">
	<form method="POST" action="{{action('ProductController@update', $product->id)}}" enctype="multipart/form-data" class="form-horizontal">
		@method('PATCH')
		@csrf
		<div class="box-body">
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Name</label>
				<div class="col-sm-3">
					<input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name') ?: $product->name}}" />
				</div>
			</div>
			<div class="form-group">
				<label for="subCategory_id" class="col-sm-3 control-label">Category</label>
				<div class="col-sm-3">
					<select name="subCategory_id" class="form-control">
						<option value="">--- Select Category ---</option>
						@if (!empty($subCategories))
						@foreach ($subCategories as $subCat)
						<option value="{{$subCat->id}}" {{old('subCategory_id') == $subCat->id ? 'selected' : $product->sub_category_id == $subCat->id ? 'selected' : ''}}>{{$subCat->parent_cat_name}} - {{$subCat->name}} </option>
						@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Description</label>
				<div class="col-sm-3">
					<textarea name="description" class="form-control" placeholder="Description">{{old('description') ?: $product->description}}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Images</label>
				<div class="col-sm-3">
					<input type="file" name="images[]" class="form-control" multiple />
				</div>
			</div>
		</div>
		<div class="box-footer">
			<a href="{{action('ProductController@index')}}" class="btn btn-info">Back to List</a>
			<input type="submit" class="btn btn-info" value="Update" />
		</div>
	</form>
</div>
@stop