@extends('app')

@section('content')
<div class="row">
	<div class="col-xs-12">
	<div class="box">
        <div class="box-header">
          <a href="{{ action('ProductController@create') }}" class="btn btn-info">Create New</a>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" width="100%">
            <thead>
              <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Sub-Category</th>
        				<th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @if (!empty($products))
                @foreach ($products as $product)
                  <tr>
                    <td>
                      {{$product->name}}
                    </td>
                    <td>
                      {{$product->subCategory->category->name}}
                    </td>
                    <td>
                      {{$product->subCategory->name}}
                    </td>
                    <td>
                      <a href="{{action('ProductController@edit', $product->id)}}" class="btn btn-info">Edit</a>
                      <a href="{{action('ProductController@show', $product->id)}}" class="btn btn-info">Details</a>
                      <form action="{{action('ProductController@destroy', $product->id)}}" method="post" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop
