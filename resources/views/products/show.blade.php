@extends('app')

@section('content')
<div class="box box-info">
    <div class="box-header with-border">
        <a href="{{action('ProductController@index')}}" class="btn btn-info">Back to List</a>
    </div>
    <div class="form-horizontal">
        <div class="box-body">
            <div class="form-group">
				<label class="col-sm-3 control-label">Name</label>
				<div class="col-sm-3">
					<label class="form-control">{{$product->name}}</label>
				</div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Category</label>
                <div class="col-sm-3">
                    <label class="form-control">{{$product->subCategory->category->name}}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Sub-Category</label>
                <div class="col-sm-3">
                    <label class="form-control">{{$product->subCategory->name}}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-3">
                    <label class="form-control">{{$product->description}}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Images</label>
                <div class="col-sm-3">
                    @foreach ($product->images as $img)
                    <p><img src="{{asset('/image/'.$img)}}" class="img-responsive" /></p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@stop

