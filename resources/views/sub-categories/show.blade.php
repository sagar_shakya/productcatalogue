@extends('app')

@section('content')
<div class="box box-info">
    <div class="box-header with-border">
        <a href="{{action('SubCategoryController@index')}}" class="btn btn-info">Back to List</a>
    </div>
    <div class="form-horizontal">
        <div class="box-body">
            <div class="form-group">
				<label class="col-sm-3 control-label">Name</label>
				<div class="col-sm-3">
					<label class="form-control">{{$subCategory->name}}</label>
				</div>
            </div>
            <div class="form-group">
                    <label class="col-sm-3 control-label">Parent Category</label>
                    <div class="col-sm-3">
                        <label class="form-control">{{$subCategory->category->name}}</label>
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-3">
                    <label class="form-control">{{$subCategory->description}}</label>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

