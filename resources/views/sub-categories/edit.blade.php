@extends('app')

@section('content')
<div class="box box-info">
	<form method="POST" action="{{action('SubCategoryController@update', $subCategory->id)}}" class="form-horizontal">
		@method('PATCH')
		@csrf
		<div class="box-body">
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Name</label>
				<div class="col-sm-3">
					<input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name') ?: $subCategory->name}}" />
				</div>
			</div>
			<div class="form-group">
					<label for="category_id" class="col-sm-3 control-label">Parent Category</label>
					<div class="col-sm-3">
						<select name="category_id" class="form-control">
							<option value="">--- Select Parent Category ---</option>
							@if (!empty($categories))
							@foreach ($categories as $id=>$name)
							<option value="{{$id}}" {{old('category_id') == $id ? 'selected' : $subCategory->category_id == $id ? 'selected' : ''}}>{{$name}}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Description</label>
				<div class="col-sm-3">
					<textarea name="description" class="form-control" placeholder="Description">{{old('description') ?: $subCategory->description}}</textarea>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<a href="{{action('SubCategoryController@index')}}" class="btn btn-info">Back to List</a>
			<input type="submit" class="btn btn-info" value="Update" />
		</div>
	</form>
</div>
@stop