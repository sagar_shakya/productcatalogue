@extends('app')

@section('content')
<div class="row">
	<div class="col-xs-12">
	<div class="box">
        <div class="box-header">
          <a href="{{ action('SubCategoryController@create') }}" class="btn btn-info">Create New</a>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" width="100%">
            <thead>
              <tr>
                <th>Name</th>
                <th>Parent Category</th>
        				<th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @if (!empty($subCategories))
                @foreach ($subCategories as $scat)
                  <tr>
                    <td>
                      {{$scat->name}}
                    </td>
                    <td>
                      {{$scat->category->name}}
                    </td>
                    <td>
                      <a href="{{action('SubCategoryController@edit', $scat->id)}}" class="btn btn-info">Edit</a>
                      <a href="{{action('SubCategoryController@show', $scat->id)}}" class="btn btn-info">Details</a>
                      <form action="{{action('SubCategoryController@destroy', $scat->id)}}" method="post" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop
