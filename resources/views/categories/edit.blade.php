@extends('app')

@section('content')
<div class="box box-info">
	<form method="POST" action="{{action('CategoryController@update', $category->id)}}" class="form-horizontal">
		@method('PATCH')
		@csrf
		<div class="box-body">
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Name</label>
				<div class="col-sm-3">
					<input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name') ?: $category->name}}" />
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-3 control-label">Description</label>
				<div class="col-sm-3">
					<textarea name="description" class="form-control" placeholder="Description">{{old('description') ?: $category->description}}</textarea>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<a href="{{action('CategoryController@index')}}" class="btn btn-info">Back to List</a>
			<input type="submit" class="btn btn-info" value="Update" />
		</div>
	</form>
</div>
@stop