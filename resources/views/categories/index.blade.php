@extends('app')

@section('content')
<div class="row">
	<div class="col-xs-12">
	<div class="box">
        <div class="box-header">
          <a href="{{ action('CategoryController@create') }}" class="btn btn-info">Create New</a>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" width="100%">
            <thead>
              <tr>
                <th>Name</th>
        				<th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @if (!empty($categories))
                @foreach ($categories as $cat)
                  <tr>
                    <td>
                      {{$cat->name}}
                    </td>
                    <td>
                      <a href="{{action('CategoryController@edit', $cat->id)}}" class="btn btn-info">Edit</a>
                      <a href="{{action('CategoryController@show', $cat->id)}}" class="btn btn-info">Details</a>
                      <form action="{{action('CategoryController@destroy', $cat->id)}}" method="post" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop
