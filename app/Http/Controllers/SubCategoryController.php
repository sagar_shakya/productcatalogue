<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategory;
use App\Category;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subCategories = SubCategory::with('category')->get();
        return view('sub-categories.index', compact('subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        return view('sub-categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|unique:sub_categories,name',
            'category_id'=>'required|exists:categories,id'
        ]);

        $subCategory = new SubCategory();
        $subCategory->name = $request->name;
        $subCategory->category_id = $request->category_id;
        $subCategory->description = $request->description;
        $subCategory->save();

        return redirect('sub-categories')->withMessage('Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subCategory = SubCategory::with('category')->get()->find($id);
        if ($subCategory) {
            return view('sub-categories.show', compact('subCategory'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subCategory = SubCategory::find($id);
        if ($subCategory) {
            $categories = Category::pluck('name', 'id')->all();
            return view('sub-categories.edit', compact('subCategory', 'categories'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subCategory = SubCategory::find($id);
        if ($subCategory) {
            $this->validate($request, [
                'name'=>'required|unique:sub_categories,name,'.$id.',id',
                'category_id'=>'required|exists:categories,id'
            ]);

            $subCategory->name = $request->name;
            $subCategory->category_id = $request->category_id;
            $subCategory->description = $request->description;
            $subCategory->save();

            return redirect('sub-categories')->withMessage('Updated Successfully');
        } else {
            return redirect('sub-categories')->withMessage('Update Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subCategory = SubCategory::find($id);

        if ($subCategory) {
            $subCategory->delete();

            return redirect('sub-categories')->withMessage('Deleted Successfully');
        } else {
            return redirect('sub-categories')->withMessage('Deletion Failed');
        }
    }
}
