<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategory;
use App\Category;
use App\Product;
use function Opis\Closure\unserialize;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(['subCategory' => function ($query) {
            $query->with('category');
        }])->get();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subCategories = SubCategory::join('categories', 'categories.id', '=', 'sub_categories.category_id')
                                            ->select('sub_categories.id', 'sub_categories.name', 'categories.name as parent_cat_name')
                                            ->orderBy('parent_cat_name')
                                            ->get();
        return view('products.create', compact('subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|unique:products,name',
            'subCategory_id'=>'required|exists:sub_categories,id',
            'images' => 'required'
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->sub_category_id = $request->subCategory_id;
        $product->description = $request->description;
        $images = [];
        if ($files=$request->file('images')) {
            foreach ($files as $file) {
                $name=pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . date('Y_m_d_H_i_s') . '.' . $file->getClientOriginalExtension();;
                $file->move('image',$name);
                $images[]=$name;
            }
        }
        $product->img_path_arr = serialize($images);
        $product->save();

        return redirect('products')->withMessage('Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with(['subCategory' => function ($query) {
            $query->with('category');
        }])->get()->find($id);
        if ($product) {
            $product->images = unserialize($product->img_path_arr);
            return view('products.show', compact('product'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        if ($product) {
            $subCategories = SubCategory::join('categories', 'categories.id', '=', 'sub_categories.category_id')
                                                ->select('sub_categories.id', 'sub_categories.name', 'categories.name as parent_cat_name')
                                                ->orderBy('parent_cat_name')
                                                ->get();
            return view('products.edit', compact('product', 'subCategories'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if ($product) {
            $this->validate($request, [
                'name'=>'required|unique:products,name,'.$id.',id',
                'subCategory_id'=>'required|exists:sub_categories,id',
            ]);
            $product->name = $request->name;
            $product->sub_category_id = $request->subCategory_id;
            $product->description = $request->description;
            $images = [];
            if ($files=$request->file('images')) {
                $oldImages = unserialize($product->img_path_arr);
                foreach ($oldImages as $img) {
                    if (File::exists('image/' . $img)) {
                        File::delete('image/' . $img);
                    }
                }
                foreach ($files as $file) {
                    $name=pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . date('Y_m_d_H_i_s') . '.' . $file->getClientOriginalExtension();;
                    $file->move('image',$name);
                    $images[]=$name;
                }
                $product->img_path_arr = serialize($images);
            }
            $product->save();

            return redirect('products')->withMessage('Updated Successfully');
        } else {
            return redirect('products')->withMessage('Update Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product) {
            if ($product->img_path_arr) {
                $oldImages = unserialize($product->img_path_arr);
                foreach ($oldImages as $img) {
                    if (File::exists('image/' . $img)) {
                        File::delete('image/' . $img);
                    }
                }
            }
            
            $product->delete();

            return redirect('products')->withMessage('Deleted Successfully');
        } else {
            return redirect('products')->withMessage('Deletion Failed');
        }
    }
}
